#!/bin/bash
echo "Running Redis in background"
redis-server --daemonize yes
echo "Running uwsgi in background"
cd /opt/commute-helper/
uwsgi --ini wsgi.ini -d yes
echo "Running celery workers"
celery -A tasks worker --loglevel=info --beat
