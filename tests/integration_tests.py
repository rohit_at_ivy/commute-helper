
from requests import get

ep = "http://localhost:8080/"


def test_agency_list_validate_http_response():
    ep_agency_list = ep+"agency_list"
    output = get(ep_agency_list).json()
    http_response = output['api_response']['http_response']
    assert http_response == 200

def test_agency_list_negative_test_wrong_end_point():
    ep_agency_list = ep+"agency_li"
    output = get(ep_agency_list).json()
    http_response = output['api_response']['http_response']
    assert http_response == 404