
from celery import Celery
from requests import get
from sf_commute_helper.jobs.get_agency_list import get_agency_list_job

app = Celery()
app.config_from_object("celery_settings")

ep = "http://localhost:8080/"

@app.task
def get_agency_list_job_controller():
    print "#"*80
    print "### Running task1 job ### "
    print "#"*80
    get_agency_list_job(ep)
