
import MySQLdb


class DB:

    def __init__(self):
        self.conn = MySQLdb.connect("commuter.cjniiyi6rqyq.us-east-1.rds.amazonaws.com",
                                    "root", "commuter", "commuter")
        self.cursor = self.conn.cursor()

    def db_get_count(self, api_name):
        try:
            sql = "select count(*) from api_stats where api_name = " + "\"" + api_name + "\""
            print sql
            self.cursor.execute(sql)
            data = self.cursor.fetchone()
            return data
        except Exception as e:
            raise

    def get_response_time(self, api_name):
        try:
            sql = "select AVG(response_time) from api_stats where api_name = " + "\"" + api_name + "\""
            print sql
            self.cursor.execute(sql)
            data = self.cursor.fetchone()
            return data
        except Exception as e:
            raise

    def db_insert_api_stats(self, api_name, response_time):
        try:
            sql = "INSERT INTO api_stats(api_name, response_time) VALUES (\"%s\", %f)" % (api_name, response_time)
            print sql
            self.cursor.execute(sql)
            self.conn.commit()
        except Exception as e:
            raise

    def run_insert_query(self, sql):
        try:
            self.cursor.execute(sql)
            self.conn.commit()
        except Exception as e:
            raise

    def run_select_query(self, sql):
        try:
            self.cursor.execute(sql)
            data = self.cursor.fetchall()
            return data
        except Exception as e:
            raise

    def db_truncate_table(self, table_name):
        sql = "truncate "+table_name
        try:
            self.cursor.execute(sql)
            self.conn.commit()
        except Exception as e:
            raise

    def db_conn_close(self):
        self.conn.close()
