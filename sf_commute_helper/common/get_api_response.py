import xmltodict
import requests


def get_api_response_in_dict(endpoint):
    try:
        response = requests.get(endpoint)
        content = response.content
        content_dict = xmltodict.parse(content)
        content_dict['http_response'] = 200
        return content_dict
    except Exception as e:
        raise
