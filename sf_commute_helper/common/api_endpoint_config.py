
agency_list = "http://webservices.nextbus.com/service/publicXMLFeed?command=agencyList"
route_list = "http://webservices.nextbus.com/service/publicXMLFeed?command=routeList&a=%s"
route_config = "http://webservices.nextbus.com/service/publicXMLFeed?command=routeConfig&a=%s&r=%s"
predictions_agency_stop = "http://webservices.nextbus.com/service/publicXMLFeed?command=predictions" \
                          "&a=%s&stopId=%d"
predictions_agency_stop_route = "http://webservices.nextbus.com/service/publicXMLFeed?command=predictions" \
                                "&a=%s&stopId=%d&routeTag=%s"
predictions_for_multi_stops = "http://webservices.nextbus.com/service/publicXMLFeed?" \
                              "command=predictionsForMultiStops&a=%s"
schedule = "http://webservices.nextbus.com/service/publicXMLFeed?command=schedule&a=%s&r=%s"
vehicle_location_requests = "http://webservices.nextbus.com/service/publicXMLFeed?" \
                            "command=vehicleLocations&a=%s&r=%s&t=%s"