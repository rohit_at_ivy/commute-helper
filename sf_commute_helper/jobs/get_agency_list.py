
from requests import get
from sf_commute_helper.common.db_util import DB


def get_agency_list_job(ep):
    print "#"*80
    print "### Running get_agency_list_job job ### "
    print "#"*80
    ep_agency_list = ep+"agency_list"
    output = get(ep_agency_list).json()
    db_obj = DB()
    db_obj.db_truncate_table("agency_list")
    for item in output['api_response']['body']['agency']:
        region_title = item['@regionTitle'] if '@regionTitle' in item else None
        tag = item['@tag'] if '@tag' in item else None
        title = item['@title'] if '@title' in item else None
        short_title = item['@shortTitle'] if '@shortTitle' in item else None
        sql = "INSERT INTO agency_list(`@regionTitle`, `@tag`, `@title`, `@shortTitle`) VALUES " \
              "(\"%s\", \"%s\", \"%s\", \"%s\")" % (region_title, tag, title, short_title)
        print sql
        db_obj.run_insert_query(sql)
    db_obj.db_conn_close()

