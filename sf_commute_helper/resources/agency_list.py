from flask_restful import Resource
from flask import jsonify
from sf_commute_helper.common import api_endpoint_config
from sf_commute_helper.common.get_api_response import get_api_response_in_dict
from sf_commute_helper.common.db_util import DB
import time


class AgencyList(Resource):

    def get(self):
        try:
            start_time = time.clock()
            db_obj = DB()
            response = get_api_response_in_dict(api_endpoint_config.agency_list)
            response_time = time.clock() - start_time
            db_obj.db_insert_api_stats("agency_list", response_time)
            db_obj.db_conn_close()
            if "Error" in response['body'].keys():
                return jsonify({'api_response': {'error_message': response['body']['Error']['#text'],
                                                 'http_response': 400}})
            return jsonify({'api_response': response})
        except Exception as e:
            print e.message
            return jsonify({'api_response': {'error_message': "Internal Error", 'http_response': 500}})


class AgencyListDB(Resource):

    def get(self):
        try:
            start_time = time.clock()
            response = {}
            agency_list = []
            db_obj = DB()
            data = db_obj.run_select_query("select * from agency_list")
            if len(data) > 0:
                for row in data:
                    region_title = row[0]
                    tag = row[1]
                    title = row[2]
                    short_title = row[3]
                    agency_hash = {'@regionTitle': region_title, '@tag': tag, '@title': title, '@shortTitle': short_title}
                    agency_list.append(agency_hash)
                    response = {'agency': agency_list}
                response['http_response'] = 200
                response_time = time.clock() - start_time
                db_obj.db_insert_api_stats("agency_list_db", response_time)
                db_obj.db_conn_close()
                return jsonify({'api_response': response})
            else:
                return jsonify({'api_response': {'error_message': "Please try after a few seconds",
                                                 'http_response': 302}})
        except Exception as e:
            print e.message
            return jsonify({'api_response': {'error_message': "Internal Error", 'http_response': 500}})
