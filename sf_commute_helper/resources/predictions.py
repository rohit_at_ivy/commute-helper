
from flask_restful import Resource
from flask import jsonify
from flask import request
from sf_commute_helper.common import api_endpoint_config
from sf_commute_helper.common.get_api_response import get_api_response_in_dict
from sf_commute_helper.common.db_util import DB
import time


class PredictionsWithAgencyStop(Resource):

    def get(self, agency_tag, stop_id):
        try:
            start_time = time.clock()
            db_obj = DB()
            url = api_endpoint_config.predictions_agency_stop % (agency_tag, stop_id)
            response = get_api_response_in_dict(url)
            response_time = time.clock() - start_time
            db_obj.db_insert_api_stats("prediction_with_agency_stop", response_time)
            db_obj.db_conn_close()
            if "Error" in response['body'].keys():
                return jsonify({'api_response': {'error_message': response['body']['Error']['#text'],
                                                 'http_response': 400}})
            return jsonify({'api_response': response})
        except Exception as e:
            print e.message
            return jsonify({'api_response': {'error_message': "Internal Error", 'http_response': 500}})


class PredictionsWithAgencyStopRoute(Resource):

    def get(self, agency_tag, stop_id, route_tag):
        try:
            start_time = time.clock()
            db_obj = DB()
            url = api_endpoint_config.predictions_agency_stop_route % (agency_tag, stop_id, route_tag)
            response = get_api_response_in_dict(url)
            response_time = time.clock() - start_time
            db_obj.db_insert_api_stats("prediction_with_agency_stop_route", response_time)
            db_obj.db_conn_close()
            if "Error" in response['body'].keys():
                return jsonify({'api_response': {'error_message': response['body']['Error']['#text'],
                                                 'http_response': 400}})
            return jsonify({'api_response': response})
        except Exception as e:
            print e.message
            return jsonify({'api_response': {'error_message': "Internal Error", 'http_response': 500}})


class PredictionMultiStops(Resource):

    def get(self, agency_name):
        try:
            start_time = time.clock()
            db_obj = DB()
            params = request.args.getlist('stops')
            base_url = api_endpoint_config.predictions_for_multi_stops % agency_name+"&"
            search_url = ''
            for i in params:
                search_url += "stops="+i+"&"
            url = base_url+search_url[:-1]
            response = get_api_response_in_dict(url)
            response_time = time.clock() - start_time
            db_obj.db_insert_api_stats("prediction_multi_stops", response_time)
            db_obj.db_conn_close()
            if "Error" in response['body'].keys():
                return jsonify({'api_response': {'error_message': response['body']['Error']['#text'],
                                                 'http_response': 400}})
            return jsonify({'api_response': response})
        except Exception as e:
            print e.message
            return jsonify({'api_response': {'error_message': "Internal Error", 'http_response': 500}})