
from flask_restful import Resource
from flask import jsonify
from sf_commute_helper.common import api_endpoint_config
from sf_commute_helper.common.get_api_response import get_api_response_in_dict
from sf_commute_helper.common.db_util import DB
import time


class Schedule (Resource):

    def get(self, agency_tag, route_tag):
        try:
            start_time = time.clock()
            db_obj = DB()
            url = api_endpoint_config.schedule % (agency_tag, route_tag)
            response = get_api_response_in_dict(url)
            response_time = time.clock() - start_time
            db_obj.db_insert_api_stats("schedule", response_time)
            db_obj.db_conn_close()
            if "Error" in response['body'].keys():
                return jsonify({'api_response': {'error_message': response['body']['Error']['#text'],
                                                 'http_response': 400}})
            return jsonify({'api_response': response})
        except Exception as e:
            print e.message
            return jsonify({'api_response': {'error_message': "Internal Error", 'http_response': 500}})
