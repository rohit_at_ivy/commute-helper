
from flask_restful import Resource
from sf_commute_helper.common.db_util import DB
from flask import jsonify


class APIStatsNoOfHits(Resource):

    def get(self, api_name):
        try:
            db_obj = DB()
            count = db_obj.db_get_count(api_name)
            db_obj.db_conn_close()
            response = {'API_NAME': api_name, 'Number_of_hits': count[0], 'http_response': 200}
            return jsonify({'api_response': response})
        except Exception as e:
            print e.message
            return jsonify({'api_response': {'error_message': "Internal Error", 'http_response': 500}})


class APIStatsResponseTime(Resource):

    def get(self, api_name):
        try:
            db_obj = DB()
            response_time = db_obj.get_response_time(api_name)
            db_obj.db_conn_close()
            response = {'API_NAME': api_name, 'Response_Time': response_time[0], 'http_response': 200}
            return jsonify({'api_response': response})
        except Exception as e:
            print e.message
            return jsonify({'api_response': {'error_message': "Internal Error", 'http_response': 500}})
