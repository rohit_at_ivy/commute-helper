FROM rohitativy/docker-commute-helper:latest
MAINTAINER Rohit Bhattacharjee "rohitativy@gmail.com"
RUN yum install -y mariadb-devel
WORKDIR /opt/commute-helper/
RUN git pull origin master
RUN pip install -r requirements.txt
ADD startup.sh /usr/local/bin/startup.sh
RUN chmod +x /usr/local/bin/startup.sh