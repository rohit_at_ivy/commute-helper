
from flask import Flask
from flask import jsonify
from flask_restful import Api
from sf_commute_helper.resources.agency_list import AgencyList
from sf_commute_helper.resources.route_list import RouteList
from sf_commute_helper.resources.route_config import RouteConfig
from sf_commute_helper.resources.predictions import PredictionsWithAgencyStop
from sf_commute_helper.resources.predictions import PredictionsWithAgencyStopRoute
from sf_commute_helper.resources.predictions import PredictionMultiStops
from sf_commute_helper.resources.schedule import Schedule
from sf_commute_helper.resources.vehicle_location_requests import VehicleLocationRequests
from sf_commute_helper.resources.api_stats import APIStatsNoOfHits
from sf_commute_helper.resources.api_stats import APIStatsResponseTime
from sf_commute_helper.resources.agency_list import AgencyListDB

application = Flask(__name__)
api = Api(application)


@application.errorhandler(404)
def end_point_not_found(e):
    return jsonify({'api_response': {'error_message': "Sorry, Nothing at this URL", 'http_response': 404}})


api.add_resource(AgencyList, '/agency_list')
api.add_resource(AgencyListDB, '/agency_list_db')
api.add_resource(RouteList, '/route_list/<string:agency_tag>')
api.add_resource(RouteConfig, '/route_config/<string:agency_tag>/<string:route_tag>')
api.add_resource(PredictionsWithAgencyStop, '/prediction_agency_stop/<string:agency_tag>/<int:stop_id>')
api.add_resource(PredictionsWithAgencyStopRoute,
                 '/prediction_agency_stop_route/<string:agency_tag>/<int:stop_id>/<string:route_tag>')
api.add_resource(PredictionMultiStops, '/prediction_multi_stops/<string:agency_name>')
api.add_resource(Schedule, '/schedule/<string:agency_tag>/<string:route_tag>')
api.add_resource(VehicleLocationRequests, '/vehicle_location_requests/'
                                          '<string:agency_tag>/<string:route_tag>/<string:epoch_time>')
api.add_resource(APIStatsNoOfHits, '/number_of_hits/<string:api_name>')
api.add_resource(APIStatsResponseTime, '/response_time/<string:api_name>')