
from celery.schedules import crontab

CELERY_TASK_SERIALIZER = 'json'
BROKER_URL = 'redis://localhost:6379/0'
CELERY_ACCEPT_CONTENT = ['json']
CELERYBEAT_SCHEDULE = {
    'every-minute_get_agency_list_job_controller': {
        'task': 'tasks.get_agency_list_job_controller',
        'schedule': crontab(minute='*/1'),
    },
}