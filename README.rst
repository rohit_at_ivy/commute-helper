sf-commute-helper
================================

Gets the details of Buses and trains.

Quickstart
----------

Run the following commands to setup your environment:

::

    $ git clone https://rohit_at_ivy@bitbucket.org/rohit_at_ivy/commute-helper.git
    $ cd commute-helper
    $ virtualenv venv or pyvenv venv # optional
    $ pip install -r requirements.txt
    $ python app.py

You will see the following in your browser:

::

    $ * Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)
    $ * Restarting with stat
    $ * Debugger is active!
    $ * Debugger pin code: 323-437-031

Running with Docker:

::

    $ git clone https://rohit_at_ivy@bitbucket.org/rohit_at_ivy/commute-helper.git
    $ cd commute-helper
    $ docker build .
    $ docker run -p 8080:8080 -d 9bb42cdcaa51 /usr/local/bin/startup.sh

    